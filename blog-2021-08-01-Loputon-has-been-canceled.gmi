# Loputon has been canceled
2021-08-01 18:58:33 +0300

Bit of sad news: I had to cancel my ARPG project "Loputon."

Loputon was going to be retro aesthetic 3D first person dungeon crawling game, with 6 player races, random generated dungeons, leveling and inventory system.

But, in the end, I realised.. It's just too much for me alone.

So I am sorry for those who waited for the project. But I felt like I was doing the project a disservice, if I was going to "force it through." I wouldn't have enjoyed making the game, thus the game wouldn't have turned out good.

I had been working on this project for 2-4 years. Yeah.

### I want to make games/things I love to make.

The codebase was a mess. The whole game project was actually called "2d_Adventure." It was supposed to be simple, top-down RPG. That then morphed into first person dungeon crawler with grid based movement, with 2D sprites as enemies. That, then again, turned into actual 3D RPG, something like Morrowind. Which I then was going to turn into simple 3D first person dungeon crawling, but without the grid movement and 3D enemies.

I actually got to the last part. But I was so exhausted of the whole thing. I didn't want to look at the project, but was constantly telling myself that if I force it through, it will be worth it.

Nah. I don't want to force myself doing things that I really don't have to. Nobody is paying me to work on Loputon and expecting results. My fans (still weirds me out I have fans lol) would understand, and they did.

I was afraid I would get shitted on by multiple people for cancelling this project. But people were very supportive and acknowledged that sometimes, you just gotta let go. "Kill your darlings" as they say.

If I had forced myself through, it would just have sucked. Bugs everywhere, codebase that was awful to maintain and add new features to, A LOT of graphical work (which I don't have money for!!), only one launch and if you fuck it up, that's pretty much that...

I cant' make this alone, I need a team. And when I got that team, I will be making the spiritual successor to Morrowind I've always dreamt of. It just may take some years.

However, it was not an useless project: I learned a lot of new things! Itemization, inventory management, saving/loading... And learning more about my own limits, which has been the most important thing I've learned in gamedev so far.

To sum it all up: The project drained me dry, thinking about it made me dislike the thought of working on it, I wasn't interested in gamedev in general at all... It just felt all kinds of wrong and on top of it, I can't just make some games alone, and that's alright.

I will move back to making smaller games and more fun projects. Look out for Artificial Rage, my open source FPS. There is also other ideas brewing, as always... And I think people might like my next one. ;)

=> https://github.com/Akselmo/Artificial-Rage Artificial Rage

This is labour of love, after all. Not labour of force. Not for me, anyway.

Thank you for understanding. <3

