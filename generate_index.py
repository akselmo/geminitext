import re
import io
import os
from pathlib import Path
import datetime
from collections import OrderedDict

#generate index first from filenames
#dates are from new to old
#get post name from filename: (\d*)-(\d*)-(\d*)-(.*).gmi
#filter - from title
#append them to index.gmi
#I know theres unnecessary repetition but i dont really care

if os.path.exists("index.gmi"):
    os.remove("index.gmi")

index_file = open('index.gmi', 'a')
#probably unnecessary but lol
blog_filename_regex = re.compile(r'blog-(\d*)-(\d*)-(\d*)-(.*).gmi')
gemini_filename_regex = re.compile(r'^(\d*)-(\d*)-(\d*)-(.*).gmi')
blog_posts = []
gemini_posts = []


def generate_posts(postdata, posts):
    if len(postdata) > 1:
        print(postdata)
        title = str(postdata[4]).replace("-"," ")
        date = datetime.date(int(postdata[1]), int(postdata[2]), int(postdata[3]))
        post = "{} {}".format(date,title)
        posts.append("=>/posts/{} {} \n\n".format(file, post))
        posts.sort(reverse=True)

for file in Path('.').glob('*.gmi'):
    if file.name == "index.gmi":
        continue
    generate_posts(blog_filename_regex.split(file.name), blog_posts)
    generate_posts(gemini_filename_regex.split(file.name), gemini_posts)

gemini_index_lines = [
        "#Aks_Dev Gemlog\n\n",
        "Welcome to my gemlog! This is reserved for more for my random thoughts.\n",
        "Thoughts that don't necessarily fit the webblog. I have yet to add the commenting system.\n"
        "If you want to comment on something, send me mail!\n",
        "=>mailto://akselmo@akselmo.dev akselmo@akselmo.dev\n",
        "=>/ Back \n\n"
]

blog_index_lines = [
        "#Aks_Dev WebBlog posts\n\n",
        "These posts are directly mirrored from my web blog.\n",
        "You can check the web blog from following link.\n",
        "=>https://akselmo.dev Aks_Dev WebBlog\n",
        "\n"
]

index_file.writelines(gemini_index_lines)
index_file.writelines(gemini_posts)
index_file.writelines(blog_index_lines)
index_file.writelines(blog_posts)


