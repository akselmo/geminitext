#Aks_Dev Gemlog

Welcome to my gemlog! This is reserved for more for my random thoughts.
Thoughts that don't necessarily fit the webblog. I have yet to add the commenting system.
If you want to comment on something, send me mail!
=>mailto://akselmo@akselmo.dev akselmo@akselmo.dev
=>/ Back 

=>/posts/2022-04-23-Setting-up-my-gemlog-and-some-work-related-musings.gmi 2022-04-23 Setting up my gemlog and some work related musings 

=>/posts/2022-04-13-Hello-gemini.gmi 2022-04-13 Hello gemini 

#Aks_Dev WebBlog posts

These posts are directly mirrored from my web blog.
You can check the web blog from following link.
=>https://akselmo.dev Aks_Dev WebBlog

=>/posts/blog-2022-05-01-Fixing-DNS-issues-in-Kubuntu-22.04.gmi 2022-05-01 Fixing DNS issues in Kubuntu 22.04 

=>/posts/blog-2022-04-24-Scared-of-programming.gmi 2022-04-24 Scared of programming 

=>/posts/blog-2022-04-01-Playing-with-Matrix-Conduit-and-Synapse.gmi 2022-04-01 Playing with Matrix Conduit and Synapse 

=>/posts/blog-2022-02-05-Why-I-find-Linux-gaming-important.gmi 2022-02-05 Why I find Linux gaming important 

=>/posts/blog-2022-01-16-My-Devterm-experience.gmi 2022-01-16 My Devterm experience 

=>/posts/blog-2021-12-18-Thoughts-on-my-PineTime.gmi 2021-12-18 Thoughts on my PineTime 

=>/posts/blog-2021-12-02-My-character-and-Matrix-channel.gmi 2021-12-02 My character and Matrix channel 

=>/posts/blog-2021-11-27-Kubuntu-multimonitor-woes-fixes.gmi 2021-11-27 Kubuntu multimonitor woes fixes 

=>/posts/blog-2021-11-20-Who-is-Linux-for.gmi 2021-11-20 Who is Linux for 

=>/posts/blog-2021-11-12-Setting-up-Kubuntu-for-gaming.gmi 2021-11-12 Setting up Kubuntu for gaming 

=>/posts/blog-2021-09-24-Im-tired-of-social-media.gmi 2021-09-24 Im tired of social media 

=>/posts/blog-2021-09-05-Cosplay-photos.gmi 2021-09-05 Cosplay photos 

=>/posts/blog-2021-08-21-Quake-Champion-on-Linux.gmi 2021-08-21 Quake Champion on Linux 

=>/posts/blog-2021-08-21-Image-Quake-Logo-Ansii.gmi 2021-08-21 Image Quake Logo Ansii 

=>/posts/blog-2021-08-13-Sennheiser-GSP-670-wireless-on-Linux.gmi 2021-08-13 Sennheiser GSP 670 wireless on Linux 

=>/posts/blog-2021-08-12-Restrict-mouse-to-first-screen-on-linux.gmi 2021-08-12 Restrict mouse to first screen on linux 

=>/posts/blog-2021-08-03-Image-Lizard-Vector.gmi 2021-08-03 Image Lizard Vector 

=>/posts/blog-2021-08-01-Loputon-has-been-canceled.gmi 2021-08-01 Loputon has been canceled 

=>/posts/blog-2021-07-28-Back-to-Linux.gmi 2021-07-28 Back to Linux 

=>/posts/blog-2021-06-12-I-gave-Linux-gaming-a-go.gmi 2021-06-12 I gave Linux gaming a go 

=>/posts/blog-2021-06-04-Raylib-3D-level-collision.gmi 2021-06-04 Raylib 3D level collision 

=>/posts/blog-2021-05-30-How-and-why-I-got-into-gamedev.gmi 2021-05-30 How and why I got into gamedev 

